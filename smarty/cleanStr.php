<?php
				Class CleanStr{
				//テキスト整形の関数
					function execute($str){
							$str = trim($str);//先頭と末尾の空白除去
								if (get_magic_quotes_gpc())
								{//マジッククォートを取得して
									$str = stripslashes($str);//エスケープされた「\」を取り除く
								}
								//$str = str_replace("");
								$admin=null;
								if(isset($_POST['pass']))
								{
									$admin=$_POST['pass'];
								} //管理者用ログイン時に投げているパスワード
								
								if(isset($admin) && $admin!=ADMINPASS)
								{//管理者はタグ可能
									$str = htmlspecialchars($str, ENT_QUOTES, 'UTF-8');//一般ユーザーはタグ禁止
									$str = str_replace("&amp;", "&", $str);//エスケープした特殊文字をもとに戻す
								}
								return str_replace(",", "&#44;", $str);//カンマ区切りのログ用にカンマをエスケープ
					}
				}
?>
