<?php
class AdminPass
{
	public $errrorMessages = null;
	function valid($pass)
	{
		if ($pass=="")
		{
			$this->errrorMessages['passEmpty'] = '管理者用パスワードを入力してください';
		}
		if (is_null($this->errrorMessages) && $pass != ADMINPASS)
		{
			$this->errrorMessages['adminpass'] = 'パスワードが違います';
		}
		return (is_null($this->errrorMessages)) ? true : false;
	}
	function getErrorMessages()
	{
		return $this->errrorMessages;
	}
}
?>