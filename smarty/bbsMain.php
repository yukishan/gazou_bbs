<?php
//BBSのメイン記事部分の表示する項目の処理
	Class Main{
		function bbsMain($page){
			$line = file(LOGFILE);
			$log = null;
			$size='';
			$start= $page * PAGEDEF;//現在の記事(古いほど数字が増える)数*7がその記事の中で一番新しいログの配列値
			$end= $start+PAGEDEF;//↑の数+7まで記事を出す。
			for($i = $start; $i < $end; $i++)
				{
					if(!isset($line[$i])  || $line[$i]=="") continue;	//ログファイルに何もないときは処理を飛ばす
					list($no,$now,$name,$email,$sub,$com,$url,$host,$pwd,$ext,$w,$h,$time,$chk,$upfile) = explode(",", $line[$i]);	//ログをカンマ区切りで抜き出し
					$upfile = trim($upfile);	//画像データパスの余計な空白削除


					$log[$i] = [
						'no' => $no,
						'day' => $now,
						'name' => $name,
						'email' => $email,
						'title' => $sub,
						'comment' => $com,
						'url' => $url,
						'host' => $host,
						'password' => $pwd,
						'fileExist' => $ext,
						'width' => $w,
						'height' => $h,
						'time' => $time,
						'check' => $chk,
						'imagefile' => $upfile,
						]; //1つの記事に表示するべき項目を連想配列化
					clearstatcache();//file_exists関数がキャッシュに組み込まれて、次のファイルの存在チェックが出来なくなるのでここでキャッシュをクリアする。
				}

			return $log;
		}
	}
?>