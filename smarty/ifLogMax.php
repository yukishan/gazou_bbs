<?php
	Class IfLogMax{
			function logCount()
			{
				$line=file(LOGFILE);
				//ログファイルが200行以上あるときの処理
				if(count($line) >= LOG_MAX)
				{
					for($d = count($line)-1; $d >= LOG_MAX-1; $d--)
					{
						list(,,,,,,,,,$ext,,,$dtime,) = explode(",", $line[$d]);//過去ログをさかのぼって配列化
						if(is_file(PATH.$dtime.$ext.".jpg") || is_file(PATH.$dtime.$ext.".png") || is_file(PATH.$dtime.$ext.".gif"))
						{  //画像データが有る時、画像の削除
							unlink(PATH.$dtime.$ext);
						}
						$line[$d] = ""; //ログ内容を削除　　←ログが200以上あるときに今まで投稿された中で最新のログから削除していく？？2018/7/10 古いログから消すように書き換え ログの書き込み直前に入れる
					}
				}
			}
	}
?>