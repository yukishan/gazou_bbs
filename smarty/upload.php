<?php
Class Upload
{
	function uploadImg($img)
	{//画像のアップロード処理
		$upfile_name = $_FILES['upfile']['name'];//画像の名前を取得
		require_once 'config.php'; //定数用プログラム呼び出し
		$dest=PATH.$upfile_name;//画像保存場所のパス+画像名
		copy($img, $dest); //xamppのテンプレートディレクトリから画像保存用ディレクトリに移動
		$tim = time(); //投稿された時間を表示
		$size = getimagesize($dest); //画像サイズを取り出して配列化。 0=画像の幅(単位:ピクセル) 1=画像の幅(単位:ピクセル) 2=画像の種類を示すフラグ(GIF=1 JPG=2 PNG=3)
		//画像データを投稿時間と画像種類に変更。
		$renameimg=null;
		if($size[2] === 1)
		{
			$renameimg = PATH.$tim.$size[2].".gif";
		}elseif($size[2] === 2)
		{
			$renameimg = PATH.$tim.$size[2].".jpg";
		}elseif($size[2] === 3)
		{
			$renameimg = PATH.$tim.$size[2].".png";
		}
		copy($dest,$renameimg);
		return $renameimg;
	}
}
?>