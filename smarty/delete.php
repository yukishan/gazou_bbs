<?php
class Delete
{
	private $line = null;
	public $totalFileSize = null;
	public $message = "";
	public $number ="";

	public function __construct()
	{
		$this->line = file(LOGFILE);
	}

	public function logList()
	{
		$imageFileName = "";
		$imageFileSize = "";

		for($i = 0; $i < count($this->line); $i++)
		{
			if(!isset($this->line[$i]) || $this->line[$i]=="") continue; //←イコールが2つないと判定されずにエラーになった
			list($no,$day,$name,$email,$title,$comment,$url,$host,$pass,,$w,$h,$time,,$img) = explode(",", $this->line[$i]);
			$imageFileName=substr($img,6,-4);
			$imageFileSize=filesize($img);
			$this->line[$i] = [
													'no'=> $no,
													'day'=> $day,
													'name'=> $name,
													'email'=> $email,
													'title'=> $title,
													'comment'=> $comment,
													'URL'=> $url,
													'host'=> $host,
													'password'=> $pass,
													'width'=> $w,
													'height'=> $h,
													'time'=> $time,
													'imageFilePath'=> $img,
													'imageFileName' => $imageFileName,
													'imageFileSize' => $imageFileSize
												];
			$this->totalFileSize += $this->line[$i]['imageFileSize'];
		}
		return $this->line;
	}

	function logDelete($delno)
	{
		$delList = file(LOGFILE);
		$result=false;
		for ($i = 0; $i < count($delList); $i++)
		{
			if(!isset($delList[$i]) || $delList[$i]=="") continue; //←イコールが2つないと判定されずにエラーになった
			list($no,,,,,,,,,,,,,,) = explode(",", $delList[$i]);
			if ($delno == $no)
			{
				$this->number = $delno;
				$delList[$i]="";
				$fp="";
				$fp = fopen(LOGFILE, "w");//ファイルを書き込み可能状態で開いて
				flock($fp, 2);//ロックを外す
				fputs($fp, implode('', $delList));//該当ラインを削除した状態の$delListを上書きする
				fclose($fp);//ファイルを閉じる
				$result=true;
			}
		}
		return $result;
	}

	function getTotalFileSize()
	{
		return $this->totalFileSize;
	}

	function getMessage()
	{
		$this->message = "NO." . $this->number . "の記事を削除しました";
		return $this->message;
	}
}
?>
