<?php
	//投稿された記事のエラーチェックと画像
	Class check{

		private $_errorMessage = null;

		function errorCheck($post){
			define("LOGFILE", 'imglog2.log');  //ログファイル。ここに投稿内容を記録
			define("LOG_MAX",  '200');  //ログファイルの最大行数
			$resalt=true;
			$errorMessage="";
			$post=$_POST;

			//フォームから記事を投稿されていない時
			if($_SERVER['REQUEST_METHOD'] != "POST") {
				$errorMessage="不正な投稿をしないで下さい";
				$resalt=false;
			}

			// 必須チェック
			$this->_checkRequire($post)
			// 長さのチェック
			$this->_checkRequire($post);

			//時間とホストの取得
			$this->tim = time();
			$host = gethostbyaddr(getenv("REMOTE_ADDR"));
			//過去のログの値を取得
			$line=file(LOGFILE);
			list($lastno,,$lname,,,$lcom,,$lhost,,,,,$ltime,) = explode(",", $line[0]);
			//直前のログのホストと投稿時間が同じなら連続投稿とみなしてエラー
			if($lhost === $host && !$tim - $ltime = 0){
				$errorMessage="連続投稿はもうしばらく時間を置いてからお願い致します";
				$resalt=false;
			}
			//入力されたテキストの整形
			require_once 'cleanStr.php';
			$clean = new cleanStr();
			$name = $clean->execute($post['name']);
			$com = $com->execute($post['com']);
			//改行文字の統一化
			$com = str_replace( "\r\n",  "\n", $com);
			$com = str_replace( "\r",  "\n", $com);
			// 連続する空行を一行にまとめる
			$com = preg_replace("^\n((　| )*\n){3,}^","\n",$com);
			//改行文字の前に<br>を代入する(今回の改行文字は"\n")
			$com = nl2br($com);
			//\nを文字列から消す。←前の処理で\nを<br>に代入したのに、この処理必要？
			//$com = str_replace("\n",  "", $com);
			//二重投稿チェック
			$reload="";
			if($name == $lname && $com == $lcom){
				$errorMessage = "二重投稿は禁止です";
				$reload = "リロード";
				$resalt=false;
			}
			//ログ行数が既に200以上ある場合
				return (is_null($this->_errorMessage)) ? true : false ;
		}

		public function getErrorMessage()
		{
			return $this->_errorMessage;
		}

		/**
		 * 必須チェック
		 */
		private function _checkRequire($post)
		{
			//文字が書き込まれていない時
			if(preg_match("/^[ |　|]*$/",$post['name'])){
				$this->_errorMessage[] ="名前が書き込まれていません";
			}
			if(preg_match("/^[ |　|\t]*$/",$post['com'])){
				$this->_errorMessage[] ="本文が書き込まれていません";
			}
		}

		private function _checkLength($post)
		{
			//本文が1000字を超える時
			if(strlen($com) > 1000){
				$errorMessage="本文が長すぎますっ！";
				$resalt=false;
			}
		}

		public function getTime(){
			return $this->tim;
		}
		public function getRenameImg(){
			return $this->renameimg;
		}
		public function getLastNumber(){
			return $this->lastNumber;
		}
	}


