<?php
	require_once 'libs/Smarty.class.php';

	$smarty = new Smarty();
	$smarty->template_dir = 'templates';
	$smarty->compile_dir = 'templates_c';

	$mode="";
	if(isset($_POST['mode']))
	{
		$mode=$_POST['mode'];
	}elseif(isset($_GET['mode']))
	{
		$mode=$_GET['mode'];
	}

	$post=$_POST;
	$file=$_FILES;

	require_once 'config.php'; //定数用プログラム呼び出し


	switch($mode)
	{
		case 'regist':
		//投稿記事のエラーチェック
			$result=null;
			$errorMessages=null;
			require_once 'errorCheck.php';
			$errorCheck = new Check;
			$result = $errorCheck->errorCheck($post);	//エラーチェック結果
			$errorMessages = $errorCheck->getErrorMessages();	//エラーメッセージを取得

			if ($result == true)
			{
				//画像アップロード処理
				$renameimg=null;
				if($file['upfile']['tmp_name'])
				{//画像が投稿されたときのみ処理
					require_once('upload.php');//全体エラー処理の後にアップロード処理をして画像データの判定
					$upload = new Upload;
					$renameimg = $upload->uploadImg($file['upfile']['tmp_name']);
					//画像データアップ処理に失敗した時
					if(!is_file($renameimg))
					{
						$result = false;
						$errorMessages['upload'] = "アップロードに失敗しました。サーバがサポートしていない可能性があります";
					}
				}
			}


			if($result == false)
			{
				$smarty->assign('errorMessages',$errorMessages);
				$smarty->display('error.html');
			}else
			{
				//エラーがない場合、ログファイルの行数チェック(最大規定をすぎていたら既存ログ削除)
				$log = file(LOGFILE);
				if(isset($log))
				{
					require_once 'ifLogMax.php';
					$ifLogMax = new IfLogMax;
					$ifLogMax->logCount();
				}
					//ログ行数を整えたら、ログの書き込みとアップロード成功メッセージを表示
					require_once 'update.php';
					$update = new UpDate;
					$message = $update->logUpdate($renameimg);
					$smarty->assign('message',$message);
					$smarty->display('uploadComp.html');
			}

			break;
		case 'admin':
			$smarty->display('admin.html');
			break;
		case 'usrdel':
			require_once 'userDelErrCheck.php';
			$userErrorCheck = new UserDelErrCheck;
			$userErrResult = $userErrorCheck->errorCheck($post);	//エラーチェック結果
			if($userErrResult == false)
			{
				$userErrorMessages = $errorCheck->getErrorMessages();	//エラーメッセージを取得
				$smarty->assign('userErrorMessages',$userErrorMessages);
				$smarty->display('error.html');
			}else
			{
				require_once 'delete.php';
				$userDel = new Delete;
				$userDelMessage = null;
				$DelResult = false;
				$DelResult = $userDel->logDelete($post['delNo']);
				if ($DelResult == true)
				{
					$userDelMessage = $userDel->getMessage();
					$smarty->assign('message',$userDelMessage);
					$smarty->display('uploadComp.html');
				}
			}
			break;
		default:
			//今までに投稿された記事のログを配列にして表示
			$log = array(); //投稿されたログの連想配列
			$page = isset($_GET['page']) ? $_GET['page'] : 0;//index.htmlで投げられた記事の数。初期値：0
				require_once 'bbsMain.php';
				$bbsMain = new Main; //過去ログの呼び出し
				$mainLog = $bbsMain->bbsMain($page);
				$smarty->assign('mainLog',$mainLog);


				$prev = $page +1;//現時点より古い記事を出すためのpostの数
				$next = $page -1;//現時点より新しい記事を出すためのpostの数
				$oldlog = null;
				$oldlog = old_log_exist($page);


			if(count($mainLog)>=7 && isset($oldlog))
			{//現ページの記事数が７に達しているか、今の記事より古いログがあれば表示
				$smarty->assign('prev',$prev);
			}
			if($next>=0)
			{//現ページより新しい記事を出す。
				$smarty->assign('next',$next);
			}

			$smarty->display('index.html');
	}

	function old_log_exist($page)
	{//今の記事のログより古いログがあるかチェック←ここ上手く行ってないので明日作り直し(2019/08/08)
		$log = file(LOGFILE);
		$nextno=null;
		$start = $page*PAGEDEF;//現在のページ数×７=現ページの最新ログの配列値

		if(isset($log[$start+PAGEDEF]))//$start+8で現在のページより古いログがあるか確認
		{
			return $log[$start+PAGEDEF];
		}
	}
?>
