<?php
class UserDelErrCheck
{
	public $errorMessages=null;
	private $logList=null;
	private $r = false;

	public function __construct()
	{
		$this->logList = file(LOGFILE);
	}

	function errorCheck($post)
	{
		//ユーザー削除の記事NOの存在チェック
		$this->numberExist($post['delNo']);
		//ユーザー削除の削除キーの正誤チェック
		$this->numberExist($post['delPwd']);
		return (is_null($this->errorMessages)) ? true : false;
	}

	private function numberExist($number)
	{//ユーザー削除のNOの存在チェック
		for($i = 0; $i<count($this->logList); $i++)
		{
			if (!isset($this->logList[$i]) || $this->logList[$i]=="") continue;
			list($no,,,,,,,,,,,,,,) = explode(",", $this->logList[$i]);
			if ($no==$number)
			{
				$this->r = true;
			}
		}
		if ($this->r==false)
		{
			$this->errorMessages['numberError']="指定された番号は投稿記事内にありません";
		}
	}

	private function passVaild($password)
	{//ユーザー削除のPASSの存在チェック
		for ($i = 0; $i > count($this->logList); $i++)
		{
			if (!isset($this->logList[$i]) || $this->logList[$i]=="") continue;
			list(,,,,,,,,$pass,,,,,,) = explode(",", $this->logList[$i]);
			if ($password==substr(md5($pass),2,8))
			{
				$this->r = true;
			}
		}
		if ($this->r==false)
		{
			$this->errorMessages['keywordError']="削除キーが正しくありません";
		}
	}

	public function getMessage()
	{
		return $this->errorMessages;
	}
}
?>