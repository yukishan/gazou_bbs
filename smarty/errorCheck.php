<?php
	//投稿された記事のエラーチェックと画像
	Class Check{
		public $errorMessages=null;
		private $cleanStr=NULL;	//文字整形クラスを呼び出すためのインスタンスプロパティ

		public function __construct()
			{//文字整形のプログラムを呼び出すためのコンストラクタ
				require_once 'cleanStr.php';
				$this->cleanStr = new CleanStr;
			}

		public function errorCheck($post){
			require_once 'config.php'; //定数用プログラム呼び出し

			//フォームから記事を投稿されていない時
			if($_SERVER['REQUEST_METHOD'] != "POST")
			{
				$this->errorMessages['injustice']="不正な投稿をしないで下さい";
				return false;
			}
			//名前存在チェック
			$this->postExist($post);
			//本文存在チェック
			$this->postExist($post);
			//本文文字数チェック
			$this->postLength($post);
			//連続投稿の禁止
			$this->continuousPost();
			//名前、本文のテキスト整形
			$name = $this->cleanStr->execute($_POST['name']);
			$comment = $this->commentClean($post);
			//二重投稿チェック
			$this->dualSubmission($name,$comment);

			return (is_null($this->errorMessages)) ? true : false;
		}

		private function postExist($post)
		{
			//文字が書き込まれていない時
			if(preg_match("/^[ |　|]*$/",$post['name']))
				{
					$this->errorMessages['name']="名前が書き込まれていません";
				}
				if(preg_match("/^[ |　|\t]*$/",$post['com']))
			{
				$this->errorMessages['com']="本文が書き込まれていません";
			}
		}

		private function postLength($post)
		{
			//本文が1000字を超える時
				if(strlen($post['com']) > 1000){
				$this->errorMessages['Length']="本文が長すぎますっ！";
			}
		}
		private function continuousPost()
		{
			//時間とホストの取得
			$tim = time();
			$host = gethostbyaddr(getenv("REMOTE_ADDR"));
			//過去のログの値を取得
			$line=file(LOGFILE);
			if(empty($line)) return;
			list($lastno,,$lname,,,$lcom,,$lhost,,,,,$ltime,) = explode(",", $line[0]);
			//直前のログのホストと投稿時間が同じなら連続投稿とみなしてエラー
			if($lhost === $host && !$tim - $ltime = 0)
			{
				$this->errorMessages['continuity']="連続投稿はもうしばらく時間を置いてからお願い致します";
			}
		}
		private function commentClean($post)
		{
			//入力された本文の整形
			$comment = $this->cleanStr->execute($post['com']);
			//改行文字の統一化
			$comment = str_replace( "\r\n",  "\n", $comment);
			$comment = str_replace( "\r",  "\n", $comment);
			// 連続する空行を一行にまとめる
			$comment = preg_replace("^\n((　| )*\n){3,}^","\n",$comment);
			//改行文字の前に<br>を代入する(今回の改行文字は"\n")
			$comment = nl2br($comment);
			//\nを文字列から消す。←前の処理で\nを<br>に代入したのに、この処理必要？
			$comment = str_replace("\n",  "", $comment);
			return $comment;
		}

		private function dualSubmission($name,$comment)
		{//二重投稿禁止チェック
			$line=file(LOGFILE);
			if(empty($line)) return;
			list(,,$lname,,,$lcom,,,,,,,,) = explode(",", $line[0]);
			if($name == $lname && $comment == $lcom){
				$this->errorMessages['dual'] = "二重投稿は禁止です";
			}
		}
		public function getErrorMessages()
		{
			return $this->errorMessages;
		}
	}


