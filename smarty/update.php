<?php
	//ログファイルへの書き込みとアップデート完了メッセージ表示
	Class UpDate
	{
		private $clearStr=NULL;	//文字整形クラスを呼び出すためのインスタンスプロパティ

		public function __construct()
		{//文字整形のプログラムを呼び出すためのコンストラクタ
			require_once 'cleanStr.php';
			$this->clearStr = new CleanStr;
		}
			function logUpdate($renameimg)
			{
				$line=file(LOGFILE);	//過去ログを配列化
				list($lastno,,,,,,,,,,,,,,) = explode(",", $line[0]);
				//$lastno = isset($line[0]) ? $line[0]['no'] : 0;	//過去ログの最新投稿NOを取得(ログの1番最初に書かれているはずなので)
				$key=1;//画像縮小表示用変数。
				$no = $lastno + 1;//最新の投稿NO
				$pass = $this->encryption();	//パス暗号化
				$now = $this->nowTime();	//現在日時取得
				$url = (isset($_POST['url'])) ? $this->cleanUrl() : "";	//URLを文字整形
				$email = (isset($_POST['email'])) ? $this->clearStr->execute($_POST['email']) : "";	//メールアドレスを文字整形
				$host = $this->getHost();	//投稿者のホスト取得
				$sub = (isset($_POST['sub'])) ? $this->clearStr->execute($_POST['sub']) : "";	//題名を文字整形
				$com = $this->cleanComment();	//本文の文字整形
				$name = $this->clearStr->execute($_POST['name']);	//名前の文字整形
				if($renameimg)
				{
					$size = getimagesize($renameimg);	//画像ファイルのサイズを取得
					$H = $size[1];	//画像高さ(単位：ピクセル)
					$W = $size[0];	//画像幅(単位：ピクセル)
					// 画像表示縮小
					if($W > MAX_W || $H > MAX_H)
					{
						$W2 = MAX_W / $W;
						$H2 = MAX_H / $H;
						($W2 <= $H2) ? $key = $W2 : $key = $H2;
						$W = $W * $key;
						$H = $H * $key;
					}
				}else
				{
					$size = array();
					$size[2] = "";
					$W = "";
					$H = "";
				}
				//投稿記事アップロード完了用メッセージ
				if ($renameimg)
				{
					$mes = "画像".$_FILES['upfile']['name']."のアップロードが成功しました";
				}else
				{
					$mes = "記事の投稿に成功しました";
				}
				$chk = (!CHECK) ? 0 : 1;//未チェックは0
				//新たにログに書き込む
				$tim = date('Y-m-d H:i:s');
				$newline = "$no,$now,$name,$email,$sub,$com,$url,$host,$pass,$size[2],$W,$H,$tim,$chk,$renameimg,\n";
				$fp = fopen(LOGFILE, "w");
				flock($fp, 2);
				fputs($fp, $newline);
				fputs($fp, implode('', $line));
				fclose($fp);
				return $mes;
			}

			private function encryption()
			{//パスワードの暗号化
				$pass = ($_POST['pwd']) ? substr(md5($_POST['pwd']),2,8) : "*";
				$pass = $this->clearStr->execute($pass);
				return $pass;
			}
			private function nowTime()
			{//現在日時と時刻を取得
				$tim = time();
				$now = gmdate("Y/m/d(D) H:i",$tim+9*60*60);
				return $now;
			}
			private function cleanUrl()
			{//URLの暗号化
				$url = preg_replace("^http://^", "", $_POST['url']);	//URLの"http://"なくしたもの
				$url = $this->clearStr->execute($url);
				return $url;
			}
			private function getHost()
			{//投稿者のホスト取得
				$host = gethostbyaddr(getenv("REMOTE_ADDR"));
				return $host;
			}
			private function cleanComment()
			{
				//入力された本文の整形
				$comment = $this->clearStr->execute($_POST['com']);
				//改行文字の統一化
				$comment = str_replace( "\r\n",  "\n", $comment);
				$comment = str_replace( "\r",  "\n", $comment);
				// 連続する空行を一行にまとめる
				$comment = preg_replace("^\n((　| )*\n){3,}^","\n",$comment);
				//改行文字の前に<br>を代入する(今回の改行文字は"\n")
				$comment = nl2br($comment);
				//\nを文字列から消す。←前の処理で\nを<br>に代入したのに、この処理必要？ ログに改行状態で入れないため
				$comment = str_replace("\n",  "", $comment);
				return $comment;
			}
	}
?>