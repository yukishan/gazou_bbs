<?php
	require_once 'libs/Smarty.class.php';

	$smarty = new Smarty();
	$smarty->template_dir = 'templates';
	$smarty->compile_dir = 'templates_c';



	$post=$_POST;
	$file=$_FILES;

	require_once 'config.php'; //定数用プログラム呼び出し


	$postAdmin="";
	if(isset($post['admin']))
	{
		$postAdmin=$post['admin'];
	}
	$adminPass="";
	if (isset($post['pass']))
	{
		$adminPass=$post['pass'];
	}

	if ($postAdmin)
	{
		if (isset($adminPass))
		{
			require_once 'adminPassValid.php';
			$pass = new AdminPass;
			$errorMessages = null;
			$result = true;
			$result = $pass->valid($adminPass);
			if ($result == false)
			{
				$errorMessages = $pass->getErrorMessages();
				$smarty->assign('errorMessages', $errorMessages);
				$smarty->display('error.html');
			}//else
			//{
		}
		if($postAdmin=="del")
		{//管理者用削除ページ
			var_dump($postAdmin);
			require_once 'delete.php';
			$adminDel = new Delete;
			$list = null;
			$totalFileSize = "";
			$message=null;
			$list = $adminDel->logList();
			if ($list)
			{
				$totalFileSize = $adminDel->getTotalFileSize();
				$totalFileSize = (int)($totalFileSize / 1024);
			}
			$smarty->assign('list',$list);
			$smarty->assign('totalFileSize', $totalFileSize);
			//$smarty->display('admindel.html');
			//記事の削除処理
			$deleatNo = "";
			if (isset($post['delno']))
			{
				$deleteNo = $post['delno'];
				//var_dump($deleteNo);
				$result=false;
				$result=$adminDel->logDelete($deleteNo);
				//var_dump($result);
				if ($result == true)
				{
					$message = $adminDel->getMessage();
				}
			}
			$smarty->assign('totalFileSize', $totalFileSize);
			$smarty->assign('message', $message);
			if (isset($totalFileSize) && !isset($message))
			{
				$smarty->display('admindel.html');
			}
			if (isset($message))
			{
				$smarty->display('uploadComp.html');
			}
		}
		if($postAdmin=="post")
		{//管理者用投稿ページ
			$adminmessage = "タグがつかえます";
			$adminpass = $adminPass;
			$smarty->assign('adminmessage', $adminmessage);
			$smarty->assign('adminpass', $adminpass);
			$smarty->display('index.html');
		}
			//}
	}

?>