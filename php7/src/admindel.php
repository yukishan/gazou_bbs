<?php
	/* 管理者削除 */
	function admindel($delno,$chkno){
	  global $pass;

	  if($chkno || $delno){
	    $line = file(LOGFILE);
	    $find = FALSE;
	    for($i = 0; $i < count($line); $i++){
	      list($no,$now,$name,$email,$sub,$com,$url,
	           $host,$pw,$ext,$w,$h,$tim,$chk,$img) = explode(',',$line[$i]);
	      if($chkno == $no){//画像チェック$chk=1に
	        $find = TRUE;
	        $line[$i] = "$no,$now,$name,$email,$sub,$com,$url,$host,$pw,$ext,$w,$h,$tim,1,$img,\n";
	        break;
	      }
	      if($delno == $no){//削除の時は空に
	        $find = TRUE;
	        $line[$i] = "";
	        break;
	      }
	    }
	    if($find){//ログ更新
	      $fp = fopen(LOGFILE, "w");
	      flock($fp, 2);
	      fputs($fp, implode('', $line));
	      fclose($fp);
	    }
	  }
	  // 削除画面を表示
	  echo "<input type=hidden name=mode value=admin>\n";
	  echo "<input type=hidden name=admin value=del>\n";
	  echo "<input type=hidden name=pass value=\"$pass\">\n";
	  echo "<center><P>削除したい記事のチェックボックスにチェックを入れ、削除ボタンを押して下さい。\n";
	  echo "<P><table border=1 cellspacing=0>\n";
	  echo "<tr bgcolor=6080f6><th>削除</th><th>記事No</th><th>投稿日</th><th>題名</th>";
	  echo "<th>投稿者</th><th>コメント</th><th>ホスト名</th><th>添付<br>(Bytes)</th>";
	  if(CHECK) echo "<th>画像<br>許可</th>";
	  echo "</tr>\n";

	  $line = file(LOGFILE);
	  $all=NULL;
	  for($j = 0; $j < count($line); $j++){
	    $img_flag = FALSE;
	    list($no,$now,$name,$email,$sub,$com,$url,
	         $host,$pw,$ext,$w,$h,$time,$chk,$img) = explode(',',$line[$j]);
	    // フォーマット
		//var_dump($now);
	    //list($now,$dmy) = preg_split('(', $now);
	    list($now,$dmy) = explode('(', $now);
	    //var_dump('$chk=');
	    //var_dump($chk);
	    if($email) $name="<a href=\"mailto:$email\">$name</a>";
	    $com = str_replace("<br />"," ",$com);
	    $com = htmlspecialchars($com, ENT_QUOTES, 'utf-8');
	    if(strlen($com) > 40) $com = substr($com,0,38) . " ...";
	    $img=trim($img);
	    // 画像があるときはリンク
	    if($ext && is_file($img)){
	      $img_flag = TRUE;
	      $clip = "<a href=\"".$img."\" target=_blank>".$time.$ext."</a>";
	      $size = filesize($img);
	      $all += $size;			//合計計算
	    }else{
	      $clip = "";
	      $size = 0;
	    }
	    $bg = ($j % 2) ? "d6d6f6" : "f6f6f6";//背景色

	    echo "<tr bgcolor=$bg><th><input type=checkbox name=del value=\"$no\"></th>";
	    echo "<th>$no</th><td><small>$now</small></td><td>$sub</td>";
	    echo "<td><b>$name</b></td><td><small>$com</small></td>";
	    echo "<td>$host</td><td align=center>$clip<br>($size)</td>\n";

	    if(CHECK){//画像チェック
	      if($img_flag && $chk == 1){
	        echo "<th><font color=red>OK</font></th>";
	      }elseif($img_flag && $chk != 1) {
	        echo "<th><input type=checkbox name=chk value=$no></th>";
	      }else{
	        echo "<td><br></td>";
	      }
	    }
	    echo "</tr>\n";
	  }
	  if(CHECK) $msg = "or許可する";

	  echo "</table><p><input type=submit value=\"削除する$msg\">";
	  echo "<input type=reset value=\"リセット\"></form>";
	  //var_dump($all);
	  $all = (int)($all / 1024);
	  echo "【 画像データ合計 : <b>$all</b> KB 】";
	  die("</center></body></html>");
	}