<?php
	/* ユーザー削除 */
	function usrdel($no,$pwd){
		//どちらかがTRUEの場合エラーを出力
	  if($no == "" || $pwd == "") error("削除Noまたはパスワードが入力漏れです");

	  $line = file(LOGFILE);
	  $flag = FALSE;
	  for($i = 0; $i<count($line); $i++){
	    list($dno,,,,,,,,$pass,$dext,,,$dtim,) = explode(",", $line[$i]);
	    //var_dump($dno);
	    //$p=substr(md5($pwd),2,8);
	    //var_dump($no);
//var_dump('@1');
	    //$noと$dnoが同じもしくはmd5(ハッシュ値（文字列を与えるとその文字列に対応するハッシュ値を32文字で返してくれる）)2文字目（正確には3文字目）から8文字目までを切り取ったものが$apassと同じ場合
	    if($no == $dno && substr(md5($pwd),2,8) == $pass){
	      $flag = TRUE;
	      $line[$i] = "";			//パスワードがマッチした行は空に
	      $delfile = PATH.$dtim.$dext;	//削除ファイル
	      break;
	    }
	  }
//var_dump('@2');
	  //var_dump($flag);
	  if(!$flag) error("該当記事が見つからないかパスワードが間違っています");
//var_dump('@3');
	  // ログ更新
	  $fp = fopen(LOGFILE, "w");
	  flock($fp, 2);
	  fputs($fp, implode('', $line));
	  fclose($fp);
//var_dump('@4');

	  if(is_file($delfile)) unlink($delfile);//削除
	}