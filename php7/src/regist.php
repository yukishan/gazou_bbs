<?php
	/* 記事書き込み */
		function regist($name,$email,$sub,$com,$url,$pwd,$upfile,$upfile_name,$admin){
			global $REQUEST_METHOD;//$REQUEST_METHODをファイル全体で有効にする。
			//var_dump($_POST);
			//var_dump($_FILES);
			//var_dump($name);
			//$_SERVER['REQUEST_METHOD']がPOSTに等しくない場合(GETでURL直打ち)エラーを表示する
			if($_SERVER['REQUEST_METHOD'] != "POST") { error("不正な投稿をしないで下さい"); }
			// フォーム内容をチェック
			//preg_match(正規表現チェック)を調べる
			//文字列が1つも取得できない(文字が入っていない)ときエラー
			if(preg_match("/^[ |　|]*$/",$name))  error("名前が書き込まれていません"); 
			if(preg_match("/^[ |　|\t]*$/",$com))  error("本文が書き込まれていません"); 
			if(preg_match("/^[ |　|]*$/",$sub))   $sub="（無題）"; 
			if(strlen($com) > 1000) error("本文が長すぎますっ！");


			//var_dump($line);
			  // 時間とIPアドレスからホスト名を取得
			  $tim = time();
			  $host = gethostbyaddr(getenv("REMOTE_ADDR"));
			  //var_dump($_POST);
			  //ログの書き込み処理が見当たらなかったので書き足し
			  //var_dump($line);
			  // 連続投稿チェック
			  $line=file(LOGFILE);
			  /*配列の各値を複数の変数に一度に代入する                 ","区切りで分割*/
			  list($lastno,,$lname,,,$lcom,,$lhost,,,,,$ltime,) = explode(",", $line[0]);
			  //$hostと$lhostまたは!$tim - $ltime = 0じゃない場合エラーを表示
			  if($host == $lhost && !$tim - $ltime = 0)
			    error("連続投稿はもうしばらく時間を置いてからお願い致します");
			  // No.とパスと時間とURLフォーマットを設定
			  $no = $lastno + 1;
			  //文字の切り出しスタート2（３文字目から）８（９文字目まで）
			  $pass = ($pwd) ? substr(md5($pwd),2,8) : "*";
			  $now = gmdate("Y/m/d(D) H:i",$tim+9*60*60);
			  $url = preg_replace("^http://^", "", $url);
			  //テキスト整形
			  $name = CleanStr($name);
			  $email= CleanStr($email);
			  $sub  = CleanStr($sub);
			  $url  = CleanStr($url);
			  $com  = CleanStr($com);
			  // 改行文字の統一。 
			  $com = str_replace( "\r\n",  "\n", $com); 
			  $com = str_replace( "\r",  "\n", $com);
			  // 連続する空行を一行
			  $com = preg_replace("^\n((　| )*\n){3,}^","\n",$com);
			  $com = nl2br($com);										//改行文字の前に<br>を代入する
			  $com = str_replace("\n",  "", $com);	//\nを文字列から消す。
			  // 二重投稿チェック
			  //$nameと$lnameまたは$comと$lcomが同じ場合エラーを表示
			  if($name == $lname && $com == $lcom)
			    error("二重投稿は禁止です<br><br><a href=gazou.php>リロード</a>");
			  // ログ行数オーバー
			  //$lineの要素の数が200以上の場合TRUE
			  if(count($line) >= LOG_MAX){
			  	//$dが$line-1,$dが199以上,$dを1ずつ引いていく
			    for($d = count($line)-1; $d >= LOG_MAX-1; $d--){
			      list($dno,,,,,,,,,$ext,,,$dtime,) = explode(",", $line[$d]);
			  //./img/,$dtime,$extがファイルか確認し、ファイルの場合TRUEでファイルを削除する
			      if(is_file(PATH.$dtime.$ext)) unlink(PATH.$dtime.$ext);
			      $line[$d] = "";
			    }
			  }
			  // アップロード処理
			  //$upfileがnoneじゃない場合
			  if($upfile != "none"){
			    //$dest = PATH.$upfile_name;
			    //var_dump($upfile);
			    //var_dump($dest);
			  //$destに./img/と$upfile_nameを代入
			    $dest=PATH.$upfile_name;
			  //$upfileと$destをコピー
			    copy($upfile, $dest);
			  //$sizeに画像サイズを取得した$destを指定し、取得した画像のサイズと形式を配列で返す
			    $size = getimagesize($dest);
			  //画像の横幅
			    $W = $size[0];
			  //画像の高さ
			    $H = $size[1];
			  //$destの後ろから3つ目がjpgの場合
			    if(substr($dest,-3) === "jpg"){
			  //$renameimgに./img/と$timと$size[画像の形式]と.jpgを代入
			       $renameimg = PATH.$tim.$size[2].".jpg";
			  //$destの後ろから3つ目がpngの場合
			    }elseif(substr($dest,-3) === "png"){
			  //$renameimgに./img/と$timと$size[画像の形式]と.pngを代入
			       $renameimg = PATH.$tim.$size[2]."png";
			  //$destの後ろから3つ目がgifの場合
			    }elseif(substr($dest,-3) === "gif"){
			  //$renameimgに./img/と$timと$size[画像の形式]と.gifを代入
			       $renameimg = PATH.$tim.$size[2]."gif";
			  //$renameimgがファイルじゃない場合、エラーを表示
			    if(!is_file($renameimg)) error("アップロードに失敗しました。サーバがサポートしていない可能性があります");
			    }
			  //$destと$renameimgをコピー
			    copy($dest,$renameimg);
			    //var_dump($img);
			    //var_dump($H);
			    //var_dump($tim.$size[2]);
			    //echo MAX_W;
			    //echo MAX_H;
			    // 画像表示縮小
			  //$Wが250より多い、$Hが250より多い、右辺か左辺どちらかがTRUEの場合
			    if($W > MAX_W || $H > MAX_H){
			  //$W2に250÷$Wを代入
			      $W2 = MAX_W / $W;
			  //$H2に250÷$Hを代入
			      $H2 = MAX_H / $H;
			  //if文?真:偽；
			  //if($W2 <= $H2){
			  //$key = $W2;
			  //}else{
			  //$key = $H2;
			  //}
			      ($W2 <= $H2) ? $key = $W2 : $key = $H2;

			      $W = $W * $key;
			      $H = $H * $key;
			    }
			    $mes = "画像 $upfile_name のアップロードが成功しました<br><br>";
			  }
			  $chk = (!CHECK) ? 0 : 1;//未チェックは0

			  $newline = "$no,$now,$name,$email,$sub,$com,$url,$host,$pass,$size[2],$W,$H,$tim,$chk,$renameimg,\n";
			  //ファイルポインタとは、ファイルの内容を読み込んだり、新しく何かを書き込んだりする際に、読み込む部分を指定したり、新たに書き始める起点を指定したりするための指定子の役割を持つ変数である。
			  //ファイルポインタ(ログファイル)を書き込みモード(w)で開く
			  $fp = fopen(LOGFILE, "w");
			  //ファイルをロックする
			  flock($fp, 2);
			  //ファイルを書き込む
			  fputs($fp, $newline);
			  fputs($fp, implode('', $line));
			  //ファイルポインタを閉じる
			  fclose($fp);
			  var_dump($admin);
			  echo $mes."画面を切り替えます";
			  //echo "<META HTTP-EQUIV=\"refresh\" content=\"2;URL=".PHP_SELF."?\">";
		}