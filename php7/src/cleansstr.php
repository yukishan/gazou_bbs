<?php
	/* テキスト整形 */
	function CleanStr($str){
		//ファイル全体で有効
	  global $admin;

	  $str = trim($str);//先頭と末尾の空白除去
	  if (get_magic_quotes_gpc()) {//￥を削除
	    $str = stripslashes($str);
	  }//無視します。

	  //等しくない場合
	  if($admin!=ADMIN_PASS){//管理者はタグ可能
	  //特殊な意味を持たない単なる文字列に変換したうえで、"と'も単なる文字として変換。
	    $str = htmlspecialchars($str, ENT_QUOTES, 'UTF-8');//タグっ禁止
	    //"&amp;"を"&"に置換する。
	    $str = str_replace("&amp;", "&", $str);//特殊文字
	  }
	  //","を"&#44;"に置換する。
	  return str_replace(",", "&#44;", $str);//カンマを変換
	}