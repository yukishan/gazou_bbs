<?php
	/* 投稿された記事部分 */
	function main(&$dat, $page=""){
		/*ファイル名を含むパスを指定*/ $line = file(LOGFILE);//読み込んだファイルの中身を配列に格納します。
		$st = ($page) ? $page : 0;//$page=0
		//1ページに出せる記事数(7)まで繰り返し
		for($i = $st; $i < $st+PAGE_DEF; $i++){
			//①ログファイルに何も入っていない時に飛ばして②の処理を進める
			  /*否定*/          /*または*/          //繰り返し処理をスキップ
			if(!isset($line[$i]) || $line[$i]=="") continue;
			//②投稿されたすべての項目をログファイルから取得してカンマで区切っている
			/*配列の各値を複数の変数に一度に代入する*/list($no,$now,$name,$email,$sub,$com,$url,$host,$pwd,$ext,$w,$h,$time,$chk,$upfile) = explode(",", $line[$i]);//","区切りで分割
			// if($ext){
			// 	echo var_dump($ext);
			// }
			// URLとメールにリンク
			if($url)   $url = "<a href=\"http://$url\" target=_blank>Link</a>";
			if($email) $name = "<a href=\"mailto:$email\">$name</a>";
			/* 置き換える文字列 , 置換後の文字列 , 対象の文字列 */
			$com = preg_replace("/(^|\/>)(&gt;[^<]*)/", "\\1<font color=789922>\\2</font>", $com);
			// 画像ファイル名
			$img = $upfile;
			/*スペースの削除*/
			$img = trim($img);
			/*対象のデータの中に指定した文字列が存在したら置換する処理*/
			//$imgNoPath = str_replace('C:/xampp/htdocs/gazou/img/','',$img);
			//$imgfile = str_replace('C:/xampp/htdocs/gazou','',$img);
			/*ファイルやURLを開く関数 r読み込みのみオープン。*/
			//$fo=fopen($img,'r');
			//$result = ($img == "./img/15296700532.jpg") ? true : false ;
			/*ファイル存在チェック*/
			//$test = file_exists($img);
			/* 自由に変更してください["]=[\"]に */
			// <imgタグ作成
			$imgsrc = "";
			//$extまたはfile_exists($img)がTRUEの場合
			if($ext && file_exists($img)){
				       //小数点以下を切り上げる
				$size = ceil(filesize($img) / 1024);//altにサイズ表示
				if(CHECK && $chk != 1){//管理人はチェックしない
					//チェック中の時の代替画像を代入
					$imgsrc = "<img src=".SOON_ICON.">";
				}elseif($w && $h){//サイズがある時

					$imgsrc = "<a href=\"".$img."\" target=_blank><img src=".$img."
								border=0 align=left width=$w height=$h hspace=20 alt=\"".$size." KB\"></a>";
				}else{//それ以外
					$imgsrc = "<a href=\"".$img."\" target=_blank><img src=".$img."
								border=0 align=left hspace=20 alt=\"".$size." KB\"></a>";
				}
			}
			//fclose($fo);
			//$test =ceil(filesize($imgfile) / 1024);
			// メイン作成
			$dat.="No.$no <font color=#cc1105 size=+1><b>$sub</b></font><br> ";
			$dat.="Name <font color=#117743><b>$name</b></font> Date $now &nbsp; $url";
			$dat.="<p><blockquote>$imgsrc $com</blockquote><br clear=left><hr>\n";
			$page++;
			clearstatcache();//ファイルのstatをクリア
		}
		$prev = $st - PAGE_DEF;
		$next = $st + PAGE_DEF;
		// 改ページ処理
		$dat.="<table align=left><tr>\n";
		if($prev > 0){
			$dat.="<td><form action=\"".PHP_SELF."\" method=POST>";
			$dat.="<input type=hidden name=page value=$prev>";
			$dat.="<input type=submit value=\"前のページ\" name=submit>\n";
			$dat.="</form></td>\n";
		}
		   //$page=7が7以上および$lineの中の要素数が7より多い場合次のページのリンクを張る
		if($page >= PAGE_DEF && count($line) > PAGE_DEF){
			$dat.="<td><form action=\"".PHP_SELF."\" method=POST>";
			$dat.="<input type=hidden name=page value=$next>";
			$dat.=" <input type=submit value=\"次のページ\" name=submit>\n";
			$dat.="</form></td>\n";
		}
		$dat.="</td>\n</tr></table>\n";
	}