<?php
/*************************************
  * 画像BBS             by ToR
  *
  * http://php.s3.to/
  *
  * 画像アップロード掲示板です。
  *
  * 保存用ディレクトリimgを作成して777にします。
  * 空のログファイルimglog.logを用意して666にします。
  * サーバーによってはアプロードできません
  *
  * 2001/09/27 v2.4 画像保存名をローカル→時間名、ページング
  * 2001/10/31 v3.0 作り直し。管理者用投稿ページ作成。ﾌｫｰﾑも分離可
  **************************************/
//----設定--------
	define("LOGFILE", 'imglog2.log');		//ログファイル名
	define("PATH", './img/');			//画像保存ディレクトリ./???/

	define("TITLE", '画像BBS');		//タイトル（<title>とTOP）
	define("HOME",  'http://php.s3.to');	//「ホーム」へのリンクhttp://php.s3.to

	define("MAX_KB", 100);			//投稿容量制限 KB（phpの設定により2Mまで
	define("MAX_W", 250);			//投稿サイズ幅（これ以上はwidthを縮小
	define("MAX_H", 250);			//投稿サイズ高さ

	define("PAGE_DEF", '7');			//一ページに表示する記事
	define("LOG_MAX",  '200');		//ログ最大行数

	define("ADMIN_PASS", '0123');		//管理者パス
	define("CHECK", 1);			//管理者がチェックしてから画像表示？yes=1
	define("SOON_ICON", 'soon.jpg');		//チェック中の時の代替画像

	define("BUNRI", 0);			//投稿フォームを分離する？

	//define("PHP_SELF", $PHP_SELF);		//このスクリプト名
	//define("PHP_SELF", $_SERVER['SERVER_NAME'].'/gazou/gazou.php');		//このスクリプト名
	define("PHP_SELF", 'gazou.php');

		require 'src/head.php';
		require 'src/form.php';
		require 'src/main.php';
		require 'src/foot.php';
		require 'src/regist.php';
		require 'src/cleansstr.php';
		require 'src/usrdel.php';
		require 'src/valid.php';
		require 'src/admindel.php';
		require 'src/error.php';

	$mode="";
	if(isset($_REQUEST['mode'])){
		$mode=$_REQUEST['mode'];
	}

	/*-----------Main-------------*/
	switch($mode){
	  case 'regist':
		$post=$_POST;
		$name=$post['name'];
		$email=$post['email'];
		$sub=$post['sub'];
		$com=$post['com'];
		$url=$post['url'];
		$pwd=$post['pwd'];
		$upfile=$_FILES['upfile']['tmp_name'];
		$upfile_name=$_FILES['upfile']['name'];
		$admin=$_POST['admin'];
	    regist($name,$email,$sub,$com,$url,$pwd,$upfile,$upfile_name,$admin);
        break;
	  case 'admin':

	    $admin="";
	    if(isset($_POST['admin'])){
	       $admin=$_POST['admin'];
	    }
	    $pass="";
	    if(isset($_POST['pass'])){
		    $pass=$_POST['pass'];
	    }

	    valid($pass);
	    //$del=;
	    $chk=range(1,32);
	    $del=NULL;
	    if(isset($_POST['del'])){
	       $del=$_POST['del'];
	    }
	    if($admin=="del") admindel($del,$chk);
	    if($admin=="post"){
	      echo "</form>";
	      form($post,1);
	      echo $post;
	      die("</body></html>");
	    }
	    break;
	  case 'usrdel':
	  $no=$_POST['no'];
	  $pwd=$_POST['pwd'];
	    usrdel($no,$pwd);
	    break;
	  default:
	    $buf="";
	    $admin="";
	    head($buf);
	    if(!BUNRI) form($buf,$admin);
	    main($buf);
	    foot($buf);
	    echo $buf;
	}
?>